# BXC_RtspClient

* 作者：北小菜 
* 官网：http://www.beixiaocai.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 作者-哔哩哔哩主页：https://space.bilibili.com/487906612
* 作者-头条西瓜主页：https://www.ixigua.com/home/4171970536803763
* github开源地址：https://github.com/any12345com/BXC_RtspClient
* gitee开源地址：https://gitee.com/Vanishi/BXC_RtspClient

### 项目介绍
~~~
BXC_RtspClient 是基于C++开发的 RTSP客户端项目

study1 《从零开发RTSP客户端》第1讲源码：RTSP信令交互全过程抓包讲解，以及信令交互全过程代码实现。
study2 《从零开发RTSP客户端》第2讲源码：解析RTP数据包中的H264码流，并写入本地文件，可以直接播放

~~~

### 依赖库RtpLibrary下载地址
* 下载地址：https://www.beixiaocai.com/code-detail/RtpLibrary


### windows系统编译运行
~~~
作者的开发环境 vs2019（vs2017,vs2022应该也都没有问题）

双击打开 BXC_RtspClient.sln，选择 x64/Debug 或 x64/Release 直接运行

~~~